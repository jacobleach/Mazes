package org.sythe.suf.maze;

import java.util.Random;
import java.util.Stack;

public class MazeFactory
{
    public static int[] createMaze(int width, int height, int endX, int endY)
    {
        return new Maze().createMaze(width, height, endX, endY);
    }
    
}
