package org.sythe.suf.maze;

import java.util.ArrayList;
import java.util.Random;

public class MazeNode
{
    private MazeNode[] neighbors;
    private ArrayList<MazeNode> removedNeighbors = new ArrayList<MazeNode>();
    private int neighborNum;
    private int x;
    private int y;
    private boolean visited = false;

    public MazeNode(int x)
    {
        this(x, 0);
    }

    public MazeNode(int x, int y)
    {
        neighbors = new MazeNode[4];
        neighborNum = 0;
        this.x = x;
        this.y = y;
    }

    public MazeNode getFirst()
    {
        for (MazeNode temp : neighbors)
        {
            if (temp != null)
            {
                return temp;
            }
        }
        return null;
    }

    public MazeNode[] getAllNeighbors()
    {
        return neighbors;
    }

    public MazeNode getRandomUnvisitedNeighbor()
    {
        Random rnd = new Random();
        return getRandomUnvisitedNeighbor(rnd);

    }

    public MazeNode getRandomUnvisitedNeighbor(Random rnd)
    {
        ArrayList<MazeNode> temp = new ArrayList<MazeNode>();

        for (int i = 0; i < neighbors.length; i++)
        {
            if (neighbors[i] != null && !neighbors[i].isVisisted())
            {
                temp.add(neighbors[i]);
            }
        }
        return (temp.size() == 0) ? null : temp.get(rnd.nextInt(temp.size()));

    }

    public void removeNeighbor(int x, int y)
    {
        for (MazeNode node : neighbors)
        {
            if (node != null && (node.getX() == x && node.getY() == y))
            {
                node.killNeighbor(getX(), getY());
                killNeighbor(x, y);
            }
        }
    }

    protected void killNeighbor(int x, int y)
    {
        for (int i = 0; i < neighbors.length; i++)
        {
            if (neighbors[i] != null && (neighbors[i].getX() == x && neighbors[i].getY() == y))
            {
                removedNeighbors.add(neighbors[i]);
                neighbors[i] = null;
            }
        }
    }

    public boolean hasUnvisitedNeighbors()
    {
        for (MazeNode node : neighbors)
        {
            if (node != null && !node.isVisisted())
            {
                return true;
            }
        }
        return false;
    }

    public boolean isVisisted()
    {
        return visited;
    }

    public void setVisited()
    {
        visited = true;
    }

    public void addNeighbor(MazeNode neighbor)
    {
        addNeighbor(neighbor, false);
    }

    public void addNeighbor(MazeNode neighbor, boolean setOther)
    {
        neighbors[neighborNum++] = neighbor;
        if (setOther)
        {
            neighbor.addNeighbor(this, false);
        }
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public String toString()
    {
        return x + "," + y + "'s neighbors are: " + ((neighbors[0] != null) ? neighbors[0].getX() + "," + neighbors[0].getY() : "") + " " + ((neighbors[1] != null) ? neighbors[1]
                .getX() + "," + neighbors[1].getY() : "") + " " + ((neighbors[2] != null) ? neighbors[2].getX() + "," + neighbors[2].getY()
                : "") + " " + ((neighbors[3] != null) ? neighbors[3].getX() + "," + neighbors[3].getY() : "");
    }
}
