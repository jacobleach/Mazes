package org.sythe.suf;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.sythe.suf.grid.ScrollableGrid;
import org.sythe.suf.maze.Maze;
import org.sythe.suf.maze.MazeFactory;


public class MazeTester
{
    static JFrame a;
    static ScrollableGrid b;
    static JScrollPane c;

    public static void main(String[] args)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                int x = 31;
                a = new JFrame();
                a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                a.setSize(700, 700);
                b = new ScrollableGrid(x, x, 20);
                c = new JScrollPane(b);
                c.setViewportBorder(BorderFactory.createLineBorder(Color.black));
                a.add(c);
                a.setVisible(true);

                Color[] colors = { Color.WHITE, Color.black, Color.red, Color.blue };
                Maze maze = new Maze(123);
                b.setAllSquares(maze.getMaze(x, x, 0, 0), colors);

                // int x = 10;
                // a = new JFrame();
                // a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                // a.setSize(700, 700);
                // b = new ScrollableGrid(9, 9, 20);
                // c = new JScrollPane(b);
                // c.setViewportBorder(BorderFactory.createLineBorder(Color.black));
                // a.add(c);
                // a.setVisible(true);
                //
                // Color[] colors = { Color.WHITE, Color.black, Color.red, Color.blue };
                // Maze maze = new Maze(123);
                // b.setAllSquares(maze.createMaze(5, 5, 0,0), colors);
            }
        });

    }
}
