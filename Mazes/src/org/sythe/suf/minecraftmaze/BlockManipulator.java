package org.sythe.suf.minecraftmaze;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

public class BlockManipulator
{
    /*
     * Todo: Make sure type is valid
     */
    public static void fill(World world, Location start, Location end, int type)
    {
        int lowX = (int) (start.getX() <= end.getX() ? start.getX() : end.getX());
        int lowY = (int) (start.getY() <= end.getY() ? start.getY() : end.getY());
        int lowZ = (int) (start.getZ() <= end.getZ() ? start.getZ() : end.getZ());

        int widthX = Math.abs(start.getBlockX() - end.getBlockX());
        int widthY = Math.abs(start.getBlockY() - end.getBlockY());
        int widthZ = Math.abs(start.getBlockZ() - end.getBlockZ());
        
        for (int countX = 0; countX <= widthX; countX++)
        {
            for (int countY = 0; countY <= widthY; countY++)
            {
                for (int countZ = 0; countZ <= widthZ; countZ++)
                {
                    Block b = world.getBlockAt(countX + lowX, countY + lowY, countZ + lowZ);
                    b.setTypeId(type);
                }
            }
        }

    }
    
    public static void fill(World world, Location start, int[][][] blocks)
    {
        for(int countX = 0; countX < blocks.length; countX++)
        {
            for (int countY = 0; countY < blocks[countX].length; countY++)
            {
                for (int countZ = 0; countZ < blocks[countX][countY].length; countZ++)
                {
                    Block b = world.getBlockAt(start.getBlockX() + countX, start.getBlockY() + countY, start.getBlockZ() + countZ);
                    b.setTypeId(blocks[countX][countY][countZ]);
                }
            }
        }
    }
    
    public static void fill(World world, Location start, int[][][] data, int... types)
    {
        for(int countX = 0; countX < data.length; countX++)
        {
            for (int countY = 0; countY < data[countX].length; countY++)
            {
                for (int countZ = 0; countZ < data[countX][countY].length; countZ++)
                {
                    Block b = world.getBlockAt(start.getBlockX() + countX, start.getBlockY() + countY, start.getBlockZ() + countZ);
                    b.setTypeId(types[data[countX][countY][countZ]]);
                }
            }
        }
    }
}
