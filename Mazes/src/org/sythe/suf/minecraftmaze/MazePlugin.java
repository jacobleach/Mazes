package org.sythe.suf.minecraftmaze;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.player.PlayerInteractEvent;

public class MazePlugin extends JavaPlugin implements Listener
{
    private MineCraftMaze maze;
    private boolean active = false;

    public void onEnable()
    {
        getServer().getPluginManager().registerEvents(this, this);
        maze = new MineCraftMaze(100, 100, 0, 0, 123);
    }

    public void onDisable()
    {

    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
    {
        if (cmd.getName().equalsIgnoreCase("spawnmaze"))
        {
            if (args.length > 1)
            {
                active = true;
                maze = new MineCraftMaze(Integer.parseInt(args[0]), Integer.parseInt(args[1]), 0, 0, 123);
            }
            return true;
        }
        return false;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void PlayerInteractEvent(PlayerInteractEvent e)
    {
        if (e.getItem().getTypeId() == 37)
        {
            maze.spawnMaze(e.getPlayer().getWorld(), e.getPlayer().getLocation(), 3, 0, 1);
        }
        active = false;
    }
}
