package org.sythe.suf.minecraftmaze;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
import org.sythe.suf.maze.Maze;

public class MineCraftMaze
{
    private static final int DEFAULT_HEIGHT = 10;
    private static final int DEFAULT_WIDTH = 10;
    private static final int DEFAULT_END_X = 0;
    private static final int DEFAULT_END_Y = 0;

    private int length;
    private int width;
    private int endX;
    private int endY;
    private long seed;
    private Maze maze;
    private int[] mazeData;

    public MineCraftMaze()
    {
        this(DEFAULT_HEIGHT, DEFAULT_WIDTH);
    }

    public MineCraftMaze(int width, int length)
    {
        this(width, length, DEFAULT_END_X, DEFAULT_END_Y);
    }

    public MineCraftMaze(int width, int length, int endX, int endY)
    {
        this(width, length, DEFAULT_END_X, DEFAULT_END_Y, new Random().nextLong());
    }

    public MineCraftMaze(int width, int length, int endX, int endY, long seed)
    {
        this.length = length;
        this.width = width;
        this.endX = endX;
        this.endY = endY;
        this.seed = seed;
        maze = new Maze(seed);
        mazeData = maze.createMaze(width, length, endX, endY);
    }

    public void spawnMaze(World world, Location start, int wallHeight, int... types)
    {
        int realWidth = (2 * width);
        int realLength = (2 * length);
        
        BlockManipulator.fill(world, new Location(world, start.getX() + 1, start.getY(), start.getZ() + 1), getMaze(wallHeight), types);
        
        // Add floor
        BlockManipulator
                .fill(world, new Location(world, start.getBlockX(), start.getBlockY() - 1, start.getBlockZ()), new Location(world,
                        start.getBlockX() + realWidth, start.getBlockY() - 1, start.getBlockZ() + realLength), types[1]);
        
        // Add walls
        BlockManipulator.fill(world, start, new Location(world, start.getBlockX() + realWidth, start.getBlockY() + wallHeight, start
                .getBlockZ()), types[1]);
        BlockManipulator
                .fill(world, new Location(world, start.getBlockX() + realWidth, start.getBlockY(), start.getBlockZ()), new Location(world,
                        start.getBlockX() + realWidth, start.getBlockY() + wallHeight, start.getBlockZ() + realLength), types[1]);
        BlockManipulator
                .fill(world, new Location(world, start.getBlockX() + realWidth, start.getBlockY(), start.getBlockZ() + realLength), new Location(
                        world, start.getBlockX(), start.getBlockY() + wallHeight, start.getBlockZ() + realLength), types[1]);
        BlockManipulator.fill(world, new Location(world, start.getBlockX(), start.getBlockY(), start.getBlockZ() + realLength), new Location(
                world, start.getBlockX(), start.getBlockY() + wallHeight, start.getBlockZ()), types[1]);

    }

    public int[][][] getMaze(int wallHeight)
    {
        int[][][] realMaze = new int[(2 * width) - 1][wallHeight][(2 * length) - 1];

        int row = -1;
        for (int i = 0; i < mazeData.length; i++)
        {
            int rowX = i % ((2 * width) - 1);// Position on the row

            if (rowX == 0)
            {
                row++;
            }
            for (int j = 0; j < wallHeight; j++)
            {
                realMaze[rowX][j][row] = mazeData[i];
            }
        }

        return realMaze;
    }
}
