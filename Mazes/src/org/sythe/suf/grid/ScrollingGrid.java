package org.sythe.suf.grid;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;

public class ScrollingGrid extends JScrollPane
{
    private ScrollableGrid grid;

    public ScrollingGrid(int x, int y, int gridWidth)
    {
        grid = new ScrollableGrid(x, y, gridWidth);
        this.setViewportView(grid);
        setViewportBorder(BorderFactory.createLineBorder(Color.black));
    }

    public Dimension getPreferredScrollableViewportSize()
    {
        return getPreferredSize();
    }

    public void setAllSquares(int[] contents, Color... colors)
    {
        grid.setAllSquares(contents, colors);
    }

    public void setSquare(int x, int y, Color squareColor)
    {
        grid.setSquare(x, y, squareColor);
    }
}
