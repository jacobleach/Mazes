package org.sythe.suf.grid;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class GridTester
{
    public static void main(String[] args)
    {
        JFrame a = new JFrame();
        a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        a.setSize(700, 700);
        ScrollableGrid b = new ScrollableGrid(900, 900, 50);
        b.setSquare(0, 0, Color.red);
        b.setSquare(0, 1, Color.blue);
        JScrollPane c = new JScrollPane(b);
        c.setViewportBorder(BorderFactory.createLineBorder(Color.black));
        a.add(c);
        a.setVisible(true);
    }

}
