package org.sythe.suf.grid;
import java.awt.Color;

public class GridSquare
{
    private int x;
    private int y;
    private Color squareColor;

    public GridSquare(int x, int y, Color squareColor)
    {
        this.x = x;
        this.y = y;
        this.squareColor = squareColor;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public Color getSquareColor()
    {
        return squareColor;
    }
}
