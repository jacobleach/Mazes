package org.sythe.suf.grid;

import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;


public class ScrollableGrid extends Grid implements Scrollable
{

    public ScrollableGrid(int width, int height, int gridWidth)
    {
        super(width, height, gridWidth);
    }

    public Dimension getPreferredScrollableViewportSize()
    {
        return getPreferredSize();
    }

    public int getScrollableBlockIncrement(Rectangle visibleRectangle, int orientation, int direction)
    {
        int currentPosition;
        if (orientation == SwingConstants.HORIZONTAL)
        {
            currentPosition = visibleRectangle.x;
        }
        else
        {
            currentPosition = visibleRectangle.y;
        }
        if (direction > 0)
        {
            int newPos = getGridWidth() - (currentPosition % getGridWidth());
            return newPos;
        }
        else
        {
            int newPosition = currentPosition % getGridWidth();
            return (newPosition == 0) ? getGridWidth() : newPosition;
        }
    }

    public boolean getScrollableTracksViewportHeight()
    {
        return false;
    }

    public boolean getScrollableTracksViewportWidth()
    {
        return false;
    }

    public int getScrollableUnitIncrement(Rectangle visibleRectangle, int orientation, int direction)
    {
        int currentPosition;
        if (orientation == SwingConstants.HORIZONTAL)
        {
            currentPosition = visibleRectangle.x;
        }
        else
        {
            currentPosition = visibleRectangle.y;
        }
        if (direction > 0)
        {
            int newPos = getGridWidth() - (currentPosition % getGridWidth());
            return newPos;
        }
        else
        {
            int newPosition = currentPosition % getGridWidth();
            return (newPosition == 0) ? getGridWidth() : newPosition;
        }
    }

}
