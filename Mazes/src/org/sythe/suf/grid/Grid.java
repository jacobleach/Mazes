package org.sythe.suf.grid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JComponent;

/**
 * @author Jacob
 * 
 */
public class Grid extends JComponent
{
    private int height;
    private int width;
    private int gridWidth;
    private int trueHeight;
    private int trueWidth;
    private GridSquare[][] squareContents;

    public Grid(int width, int height, int gridWidth)
    {
        this.height = height;
        this.width = width;
        this.gridWidth = gridWidth + 1;
        trueHeight = this.gridWidth * height;
        trueWidth = this.gridWidth * width;
        setPreferredSize(new Dimension(trueWidth + 1, trueHeight + 1));
        squareContents = new GridSquare[width][height];
    }

    public void paintComponent(Graphics g)
    {
        for (int i = 0; i <= height; i++)
        {
            g.drawLine(0, i * gridWidth, trueWidth, i * gridWidth);
        }
        for (int i = 0; i <= width; i++)
        {
            g.drawLine(i * gridWidth, 0, i * gridWidth, trueHeight);
        }
        for (GridSquare[] current : squareContents)
        {
            for (GridSquare paint : current)
            {
                if (paint != null)
                {
                    g.setColor(paint.getSquareColor());
                    /*
                     * The true coordinates must be one further as to not overlap the border. The
                     * width
                     * must
                     * be one less as well.
                     */
                    int xTrue = paint.getX() * gridWidth + 1;
                    int yTrue = paint.getY() * gridWidth + 1;
                    g.fillRect(xTrue, yTrue, gridWidth - 1, gridWidth - 1);
                }
            }
        }
    }

    /************************ SETTERS *****************************/

    public void setSquare(int x, int y, Color squareColor)
    {
        squareContents[x][y] = null; // Make sure its gone
        squareContents[x][y] = new GridSquare(x, y, squareColor);
    }
    
    public void setAllSquares(int[][] contents, Color... colors)
    {
        for (int i = 0; i < contents.length; i++)
        {
            for (int j = 0; j < contents[i].length; j++)
            {
                if (!(contents[i][j] >= colors.length - 1) && !(contents[i][j] < 0))
                {
                    setSquare(i, j, colors[contents[i][j]]);
                }
            }
        }
       
    }

    public void setAllSquares(int[] contents, Color... colors)
    {
        int row = -1;
        for (int i = 0; i < contents.length; i++)
        {
            int rowPos = i % width;// Position on the row

            if (rowPos == 0)
            {
                row++;
            }
            if (!(contents[i] >= colors.length) && !(contents[i] < 0))
            {
                setSquare(rowPos, row, colors[contents[i]]);
            }
        }
    }

    /************************ GETTERS *****************************/

    public int getGridWidth()
    {
        return gridWidth;
    }
}
